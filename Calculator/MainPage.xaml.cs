﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Calculator
{
    /* For state I took Reference from herehttps://gist.github.com/Samirgc/7df64c8253ce537c0147fa7dc9c8893c#file-mainpage-xaml-cs */
    public partial class MainPage : ContentPage
    {
        int FSA = 1;
        string operatorused;
        double numberOne, numberTwo;

        public MainPage()
        {
            InitializeComponent();
            OnClear(this, null);
        } 

        //based on the state it stores the number one and number two.
        void NumberSelected(object sender, EventArgs e)
        {
            Button button1 = (Button)sender;
            string pressedvalue = button1.Text;
            if (this.resultText.Text == "0" || FSA < 0)
            {
                this.resultText.Text = "";
                if (FSA < 0)
                    FSA *= -1;
            }

            this.resultText.Text += pressedvalue;
            double number;
            if (double.TryParse(this.resultText.Text, out number))
            {
                this.resultText.Text = number.ToString("N0");
                if (FSA == 1)
                {
                    numberOne = number;
                }
                else
                {
                    numberTwo = number;
                }
            }
        }
        //Sets state to -2 when operator is Clickked and store the opertaor in variable
        void Hand_Click_Symbols(object sender, EventArgs e)
        {
            FSA = -2;
            Button button = (Button)sender;
            string pressed = button.Text;
            operatorused = pressed;
        }
        // When AC is used this function is called to reset values
        void OnClear(object sender, EventArgs e)
        {
            numberOne = 0;
            numberTwo = 0;
            FSA = 1;
            this.resultText.Text = "0";
        }
       // Final calculation is done using  this function. We will call  another class solver and pass 3 paramters  which has 3 values two number s and one operator
        void OnCalculate(object sender, EventArgs e)
        {
            if (FSA == 2)
            {
                //calling solver class method to and use switch case back there.
                var result = Solver.Calculate(numberOne, numberTwo, operatorused);

                this.resultText.Text = result.ToString();
                numberOne = result;
                FSA = -1;
            }
        }
    }
}